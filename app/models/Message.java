package models;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import play.db.jpa.GenericModel;
import play.db.jpa.Model;

@Entity
public class Message extends Model {

	public String messageText;
	public String subject;
	public Date postedAt;

	@ManyToOne
	public User from;

	@ManyToOne
	public User to;

	public Message(User from, User to, String messageText, String subject) {

		this.from = from;
		this.to = to;
		this.messageText = messageText;
		this.subject = subject;
		postedAt = new Date();

	}

	public void displayMessage() {
		System.out.println(toString());
	}

	@Override
	public String toString() {
		return this.from.firstName + " says... " + this.messageText;
	}

}
