package models;

import java.util.List;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import javax.persistence.OneToMany;
import javax.persistence.Table;

import controllers.SortByDateComparator;

import javax.persistence.CascadeType;
import javax.persistence.Entity;

import play.db.jpa.Model;
import play.db.jpa.Blob;

@Entity
public class User extends Model {

	public String firstName;
	public String lastName;
	public String email;
	public String password;
	public String age;
	public String nationality;
	public String statusText;
	public Blob profilePicture;

	@OneToMany(mappedBy = "sourceUser")
	public List<Friendship> friendships = new ArrayList<Friendship>();

	@OneToMany(mappedBy = "to")
	public List<Message> inbox = new ArrayList<Message>();

	@OneToMany(mappedBy = "from")
	public List<Message> outbox = new ArrayList<Message>();

	public User(String firstName, String lastName, String email, String age,
			String nationality, String password) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.age = age;
		this.nationality = nationality;
		this.password = password;

	}

	public static User findByEmail(String email) {
		return find("email", email).first();
	}

	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}

	public void befriend(User friend) {
		Friendship friendship = new Friendship(this, friend);
		friendships.add(friendship);
		friendship.save();
		save();
	}

	public void unfriend(User friend) {
		Friendship thisFriendship = null;

		for (Friendship friendship : friendships) {
			if (friendship.targetUser == friend) {
				thisFriendship = friendship;
			}
		}
		friendships.remove(thisFriendship);
		thisFriendship.delete();
		save();
	}

	public void sendMessage(User to, String messageText, String subject) {
		Message message = new Message(this, to, messageText, subject);
		outbox.add(message);
		to.inbox.add(message);
		message.save();
	}

	public static HashSet<User> messageSenders(List<Message> inbox,
			List<Message> outbox) {
		HashSet<User> friendies = new HashSet<User>();

		for (Message mess : inbox) {

			friendies.add(mess.from);

		}

		for (Message mess : outbox) {

			friendies.add(mess.to);

		}

		return friendies;
	}

	public boolean friendshipsContains(User friend) {
		for (Friendship f : friendships) {
			if (f.targetUser == friend)
				return true;
		}

		return false;

	}

	public static ArrayList<Message> getConversation(User user, User friend) {

		ArrayList<Message> conversation = new ArrayList<Message>();

		for (Message m : user.outbox) {
			if (m.to.equals(friend)) {
				conversation.add(m);
			}
		}

		for (Message m : user.inbox) {
			if (m.from.equals(friend)) {
				conversation.add(m);
			}
		}

		Collections.sort(conversation, new SortByDateComparator());

		return conversation;
	}

	public String toString() {
		return firstName + " " + lastName;

	}

}
