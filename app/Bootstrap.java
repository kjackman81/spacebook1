import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import play.db.jpa.Blob;
import play.*;
import play.jobs.*;
import play.libs.MimeTypes;
import play.test.*;

import models.*;

@OnApplicationStart
public class Bootstrap extends Job {

	public void doJob() {
		Fixtures.deleteDatabase();
		Fixtures.loadModels("data.yml");

		String photoName = "homer.gif";
		Blob blob = new Blob();
		try {
			blob.set(new FileInputStream(photoName),
					MimeTypes.getContentType(photoName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		User homer = User.findByEmail("homer@simpson.com");
		homer.profilePicture = blob;
		homer.save();
		
		

		String photoName1 = "marge.gif";
		Blob blob1 = new Blob();
		try {
			blob1.set(new FileInputStream(photoName1),
					MimeTypes.getContentType(photoName1));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		User marge = User.findByEmail("marge@simpson.com");
		marge.profilePicture = blob1;
		marge.save();
		
		

		String photoName2 = "bart.gif";
		Blob blob2 = new Blob();
		try {
			blob2.set(new FileInputStream(photoName2),
					MimeTypes.getContentType(photoName2));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		User bart = User.findByEmail("bart@simpson.com");
		bart.profilePicture = blob2;
		bart.save();
		
		

		String photoName3 = "lisa.gif";
		Blob blob3 = new Blob();
		try {
			blob3.set(new FileInputStream(photoName3),
					MimeTypes.getContentType(photoName3));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		User lisa = User.findByEmail("lisa@simpson.com");
		lisa.profilePicture = blob3;
		lisa.save();
		
		

		String photoName4 = "maggie.gif";
		Blob blob4 = new Blob();
		try {
			blob4.set(new FileInputStream(photoName4),
					MimeTypes.getContentType(photoName4));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		User maggie = User.findByEmail("maggie@simpson.com");
		maggie.profilePicture = blob4;
		maggie.save();
		
		

		String photoName5 = "barney.gif";
		Blob blob5 = new Blob();
		try {
			blob5.set(new FileInputStream(photoName5),
					MimeTypes.getContentType(photoName5));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		User barney = User.findByEmail("barney@gumble.com");
		barney.profilePicture = blob5;
		barney.save();
		
		

		String photoName6 = "mo.gif";
		Blob blob6 = new Blob();
		try {
			blob6.set(new FileInputStream(photoName6),
					MimeTypes.getContentType(photoName6));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		User mo = User.findByEmail("mo@sizlack.com");
		mo.profilePicture = blob6;
		mo.save();

	}
}