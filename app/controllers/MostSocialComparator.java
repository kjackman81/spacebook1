package controllers;

import java.util.Comparator;
import java.util.List;
import models.User;

public class MostSocialComparator implements Comparator<User> {

	@Override
	public int compare(User f1, User f2) {

		//return Integer.compare(f2.friendships.size(), f1.friendships.size());
		
		if (f1.friendships.size() < f2.friendships.size())
            return -1;
        else if (f1.friendships.size() == f2.friendships.size())
            return 0;
        else 
            return 1;

	}
}