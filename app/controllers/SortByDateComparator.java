package controllers;

import java.util.Comparator;

import models.Message;

public class SortByDateComparator implements Comparator<Message> {

	public int compare(Message a, Message b) {

		return b.postedAt.compareTo(a.postedAt);

	}

}
