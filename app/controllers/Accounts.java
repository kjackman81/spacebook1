package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class Accounts extends Controller {

	public static void signup() {
		render();
	}

	public static void login() {
		render();
	}

	public static void logout() {
		session.clear();
		index();
	}

	public static void index() {
		render();
	}

	public static void register(String firstName, String lastName, String email,
			String age, String nationality, String password) {
		Logger.info(firstName + " " + lastName + " " + email + " " + age + " "
				+ nationality + " " + password);

		User user = new User(firstName, lastName, email, age, nationality,
				password);
		user.save();

		index();
	}

	public static void authenticate(String email, String password) {
		Logger.info(
				"Attempting to authenticate with " + email + ":" + password);

		User user = User.findByEmail(email);
		if ((user != null) && (user.checkPassword(password) == true)) {
			Logger.info("You are good to GO!!!:  " + user.firstName + " "
					+ user.lastName);
			session.put("logged_in_userid", user.id);
			Home.index();
		} else {
			Logger.info("Authentication failed");
			login();
		}
	}

	public static void editDetails() {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		render(user);
	}

	public static void amend(String firstName, String lastName, String email,
			String age, String nationality, String password) {

		String userId = session.get("logged_in_userid"); 
		User user = User.findById(Long.parseLong(userId));

		if (user.checkPassword(password) == true) {
			user.firstName = (firstName.isEmpty()) ? user.firstName : firstName;

			user.lastName = (lastName.isEmpty()) ? user.lastName : lastName;

			user.email = (email.isEmpty()) ? user.email : email;

			user.age = (age.isEmpty()) ? user.age : age;

			user.nationality = (nationality.isEmpty())
					? user.nationality
					: nationality;
			user.save();

			Home.index();
			Logger.info("You just got amended   :" + user.firstName + " "
					+ user.lastName);
		}

		else {
			Logger.info("Authentication failed unable to edit your details");
			Home.index();
		}
		user.save();

	}

}
