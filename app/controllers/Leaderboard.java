package controllers;
import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class Leaderboard extends Controller {

	public static void index() {

		List<User> users = User.findAll();
		render(users);

	}

	public static void social() {

		List<User> listOfFriendShips = new ArrayList<User>();

		List<User> users = User.findAll();

		for (User u : users)

		{
			listOfFriendShips.add(u);
		}

		Collections.sort(listOfFriendShips, new MostSocialComparator());

		render(listOfFriendShips);

	}

	public static void talkative() {

		List<User> amountOfMess = new ArrayList<User>();
		List<User> users = User.findAll();

		for (User u : users)

		{
			amountOfMess.add(u);
		}

		Collections.sort(amountOfMess, new MostTalkComparator());

		render(amountOfMess);

	}

}
