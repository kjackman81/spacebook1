package controllers;

import java.util.Comparator;

import models.Message;

public class SortByUserComparator implements Comparator<Message> {

	@Override
	public int compare(Message arg0, Message arg1) {

		return arg0.from.firstName.compareToIgnoreCase(arg1.from.firstName);

	}

}