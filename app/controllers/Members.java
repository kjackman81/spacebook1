package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Members extends Controller {

	public static void index() {
		String userId = session.get("logged_in_userid");
		User me = User.findById(Long.parseLong(userId));

		List<User> users = User.findAll();

		if (users.contains(me)) {
			users.remove(me);
			Logger.info("Removed " + me + " from list");
		}

		render(users);

	}

	public static void follow(Long id) {

		User friend = User.findById(id);

		String userId = session.get("logged_in_userid");
		User me = User.findById(Long.parseLong(userId));

		if (me.friendshipsContains(friend)) {
			Logger.info("You attempted to befriend " + friend.firstName
					+ " who is already a friend");
			Home.index();
		} else {
			me.befriend(friend);
			Home.index();
		}

	}

	public static void searchNotFound() {
		render();
	}

	public static void search(String name) {

		List<User> users = User.findAll();
		List<User> searchs = new ArrayList();

		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).firstName.equalsIgnoreCase(name)
					|| users.get(i).lastName.equalsIgnoreCase(name)) {
				Logger.info("name  " + users.get(i).firstName);
				searchs.add(users.get(i));

			}

		}

		if (searchs.size() < 1) {
			searchNotFound();
		}

		render(searchs);
	}
}
