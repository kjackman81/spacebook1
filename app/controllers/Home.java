package controllers;

import play.*;
import play.mvc.*;
import play.mvc.Scope.Session;

import java.util.*;

import models.*;

public class Home extends Controller {

	public static void index() {

		String userId = session.get("logged_in_userid");

		Logger.info("userId:  " + userId);

		if (userId != null && (!userId.isEmpty())) {
			User user = User.findById(Long.parseLong(userId));
			render(user);
		}

		Accounts.login();
	}

	public static void drop(Long id) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));

		User friend = User.findById(id);

		user.unfriend(friend);
		Logger.info("Dropping " + friend.email);
		index();
	}

	public static void sortByUser() {

		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));

		Collections.sort(user.inbox, new SortByUserComparator());
		render(user);

	}

	public static void sortByConversation() {

		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		ArrayList<ArrayList<Message>> conversations = new ArrayList<ArrayList<Message>>();

		HashSet<User> messageSenders = User.messageSenders(user.inbox,
				user.outbox);

		for (User u : messageSenders) {
			conversations.add(User.getConversation(user, u));
		}

		render(user, conversations);

	}

	public static void sortByDate() {

		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));

		Collections.sort(user.inbox, new SortByDateComparator());
		render(user);

	}

}
