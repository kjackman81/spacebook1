package controllers;

import java.util.Comparator;
import java.util.List;

import models.User;

public class MostTalkComparator implements Comparator<User> {

	@Override
	public int compare(User f1, User f2) {

		//return Integer.compare(f2.outbox.size(), f1.outbox.size());
		
		
		if (f1.outbox.size() < f2.outbox.size())
            return -1;
        else if (f1.outbox.size() == f2.outbox.size())
            return 0;
        else 
            return 1;
	}
}